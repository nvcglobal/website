---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
---

<p>
  With a platform like Global Nonviolent Communication (NVC), we can influence the direction of the community with the integrity that we have.
</p>
<p>
  It’s an incredible platform which allows us to have the space to do so many different things, from groups and direct messaging, to news, courses and comments.
</p>
<p>
  Today, we don’t actually know how many people are part of the global NVC community.
</p>
<p>
  Our current goal is to show that we are a million members strong. And if that seems impossible, let me tell you this to reassure you: it’s not!

  How?
</p>

<p class="text-center">
  <a class="btn btn-primary btn-lg" href="http://community.nvc.global" target="_blank">Register yourself</a>
</p>

<p class="text-center">
Get everyone you know to register on our global NVC community!
</p>

<p class="m-4 text-center">
  <a href="/worldpeace">World Peace and NVC</a>
</p>
