---
layout: page
title: World Peace
permalink: /worldpeace/
---

<div class="wordlpeace">

  We are in the process of collecting all the social change work done in the last 50 years in NVC, if you are part of any and would like to contribute, join the discussion and <a href="http://community.nvc.global" target="_blank">share your story</a>.

  <ul class="post-list">
    {% for post in site.posts %}
      <li>
        {% assign date_format = site.minima.date_format | default: "%b %-d, %Y" %}
        <span class="post-meta">{{ post.date | date: date_format }}</span>

        <a class="post-link" href="{{ post.url | relative_url }}">{{ post.title | escape }}</a>

      </li>
    {% endfor %}
  </ul>

</div>
